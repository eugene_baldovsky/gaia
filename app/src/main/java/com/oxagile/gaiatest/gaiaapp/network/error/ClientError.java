package com.oxagile.gaiatest.gaiaapp.network.error;

import com.oxagile.gaiatest.gaiaapp.R;
import com.oxagile.gaiatest.gaiaapp.app.GaiaApp;

/**
 * Client connection errors: 4xx codes.
 */
public class ClientError extends ConnectionError {
    public ClientError() {
        super("Client error");
    }

    public ClientError(int errorCode) {
        super(errorCode);
    }

    /*
    * return error message related to this error type
    * messages can be customized in according with your server implementation
    */
    @Override
    protected String getErrorMessageByCode(int errorCode){
        String message;
        switch(errorCode){
            case 400:
                message = "Bad Request";
                break;
            case 404:
                message = "Request not found";
                break;
            case 405:
                message = "Method Not Allowed";
                break;
            case 406:
                message = "Request Timeout";
                break;
            // all other code by the same scenario

            default:
                message = "Client error";
        }

        return message;
    }
}
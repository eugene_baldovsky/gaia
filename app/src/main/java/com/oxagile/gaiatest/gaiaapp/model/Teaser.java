package com.oxagile.gaiatest.gaiaapp.model;

import java.util.List;

/**
 * Contains data from <teaser>
 */

public class Teaser {
    private String value;

    public String getValue() {
        return value;
    }
}

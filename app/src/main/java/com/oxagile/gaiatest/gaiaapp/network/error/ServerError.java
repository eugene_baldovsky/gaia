package com.oxagile.gaiatest.gaiaapp.network.error;

import com.oxagile.gaiatest.gaiaapp.R;
import com.oxagile.gaiatest.gaiaapp.app.GaiaApp;

/**
 * Server connection errors: 5xx codes
 */
public class ServerError extends ConnectionError {
    public ServerError() {
        super();
    }

    public ServerError(int errorCode) {
        super(errorCode);
    }
    /*
    * return error message related to this error type
    */
    @Override
    protected String getErrorMessageByCode(int errorCode){
        String message;
        switch(errorCode){
            case 500:
                message = "Bad Request";
                break;
            case 502:
                message = "Bad Gateway";
                break;
            case 503:
                message = "Service Unavailable";
                break;
            case 598:
                message = "Network read timeout error";
                break;
            // all other code by the same scenario

            default:
                message = "Server error";
        }

        return message;
    }
}

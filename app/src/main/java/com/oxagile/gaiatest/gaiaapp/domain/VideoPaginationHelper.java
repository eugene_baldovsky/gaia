package com.oxagile.gaiatest.gaiaapp.domain;

public class VideoPaginationHelper {
    private final int itemsOnPage;
    private int currentPage;
    private int totalItems;

    public VideoPaginationHelper(int itemsOnPage) {
        this.itemsOnPage = itemsOnPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public int getItemsOnPage() {
        return itemsOnPage;
    }

    public boolean isLoadingAvailable() {
        return currentPage < availablePages() - 1;
    }

    private int availablePages() {
        int filledPages = totalItems / itemsOnPage;
        int rest = totalItems % itemsOnPage;
        return rest == 0 ? filledPages : filledPages + 1;
    }

    public int getNextPage() {
        return currentPage + 1;
    }
}

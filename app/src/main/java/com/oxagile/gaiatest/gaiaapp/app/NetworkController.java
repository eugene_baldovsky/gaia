package com.oxagile.gaiatest.gaiaapp.app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.oxagile.gaiatest.gaiaapp.R;
import com.oxagile.gaiatest.gaiaapp.network.error.NoInternetConnectionError;
import com.oxagile.gaiatest.gaiaapp.utils.AppUtils;

/**
 * Network
 */
public class NetworkController {

    private static NetworkController instance;
    private RequestQueue requestQueue;
    private Context context;
    private boolean isInternetConnected = false;

    private NetworkController(Context context) {
        this.context = context;
    }

    public static NetworkController getInstance(Context context) {
        if(instance == null) {
            synchronized (NetworkController.class){
                if(instance == null){
                    instance = new NetworkController(context);
                }
            }
        }
        return instance;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        if(isInternetConnected()) {
            getRequestQueue().add(req);
        } else {
            AppUtils.showMessage(R.string.msg_noconnection);
            Response.ErrorListener errorListener = req.getErrorListener();
            if(errorListener != null)
                errorListener.onErrorResponse(new NoInternetConnectionError());
        }
    }

    public void clearCache() {
        getRequestQueue().getCache().clear();
    }

    public void checkIntenetConnection(){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            setInternetConnected(false);
        } else
            setInternetConnected(true);
    }

    public boolean isInternetConnected() {
        return isInternetConnected;
    }

    public void setInternetConnected(boolean isConnected) {
        isInternetConnected = isConnected;
    }

    public void cancelByTag(String tag) {
        getRequestQueue().cancelAll(tag);
    }

    private RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }
}

package com.oxagile.gaiatest.gaiaapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Contains data from <fields>
 */
public class Fields {
    private List<Teaser> teaser;
    private List<StringValueItem> season;
    private List<StringValueItem> episode;
    @SerializedName("video_lang")
    private List<StringValueItem> videoLang;

    public List<Teaser> getTeaser() {
        return teaser;
    }

    public List<StringValueItem> getSeason() {
        return season;
    }

    public List<StringValueItem> getEpisode() {
        return episode;
    }

    public List<StringValueItem> getVideoLang() {
        return videoLang;
    }

}

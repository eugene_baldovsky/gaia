package com.oxagile.gaiatest.gaiaapp.model;

/**
 * Contains data for object with "value" field as a string
 */

public class StringValueItem {
    private String value;

    public String getValue() {
        return value;
    }
}

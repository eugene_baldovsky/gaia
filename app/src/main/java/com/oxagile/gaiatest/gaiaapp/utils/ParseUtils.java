package com.oxagile.gaiatest.gaiaapp.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * functions for json parsing
 */
public final class ParseUtils {
    private static final Gson GSON = new Gson();

    public static <T> T fromJson(String json, Class<T> classOfT) throws JsonSyntaxException {
        return GSON.fromJson(json, classOfT);
    }
}

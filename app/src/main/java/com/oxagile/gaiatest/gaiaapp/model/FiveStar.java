package com.oxagile.gaiatest.gaiaapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Contains data from <fivestar>
 */

public class FiveStar {
    @SerializedName("up_count")
    private NumberValueItem upCount;
    @SerializedName("down_count")
    private NumberValueItem downCount;

    public NumberValueItem getUpCount() {
        return upCount;
    }

    public NumberValueItem getDownCount() {
        return downCount;
    }
}

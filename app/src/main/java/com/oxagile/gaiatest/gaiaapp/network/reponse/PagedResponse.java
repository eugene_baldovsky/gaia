package com.oxagile.gaiatest.gaiaapp.network.reponse;

public class PagedResponse {
    private int currentPage;
    private int totalCount;

    public int getCurrentPage(){
        return currentPage;
    }

    public int getTotalCount(){
        return totalCount;
    }
}

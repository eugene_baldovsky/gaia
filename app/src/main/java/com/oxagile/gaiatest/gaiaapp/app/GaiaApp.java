package com.oxagile.gaiatest.gaiaapp.app;

import android.app.Application;

/**
 * Main application class
 */
public class GaiaApp extends Application {
    private static GaiaApp instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        NetworkController.getInstance(this).checkIntenetConnection(); //check internet connection state: online/offline
    }

    public static GaiaApp getInstance() {
        return instance;
    }


}

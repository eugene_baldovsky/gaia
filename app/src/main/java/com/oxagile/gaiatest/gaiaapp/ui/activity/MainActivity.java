package com.oxagile.gaiatest.gaiaapp.ui.activity;

import android.Manifest;
import android.app.Application;
import android.app.SearchManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.oxagile.gaiatest.gaiaapp.R;
import com.oxagile.gaiatest.gaiaapp.app.NetworkController;
import com.oxagile.gaiatest.gaiaapp.domain.VideoPaginationHelper;
import com.oxagile.gaiatest.gaiaapp.model.Preview;
import com.oxagile.gaiatest.gaiaapp.network.reponse.SearchResponse;
import com.oxagile.gaiatest.gaiaapp.network.request.SearchRequest;
import com.oxagile.gaiatest.gaiaapp.ui.adapter.MoviesAdapter;
import com.oxagile.gaiatest.gaiaapp.ui.decorator.VerticalSpaceItemDecoration;
import com.oxagile.gaiatest.gaiaapp.ui.listener.OnLoadMoreListener;
import com.oxagile.gaiatest.gaiaapp.utils.AppUtils;

/**
 * Main class of Gaia applicatoin
 */
public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";
    private static final int ITEMS_ON_PAGE = 8;
    private static final int COLUMNS_ON_PAGE = 1;

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private SearchView searchView;
    private MenuItem searchItem;
    private String queryTitle;
    private MoviesAdapter moviesAdapter;
    private VideoPaginationHelper paginationHelper = new VideoPaginationHelper(ITEMS_ON_PAGE);
    private NetworkController networkController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        networkController = NetworkController.getInstance(MainActivity.this);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshList);
        refreshLayout.setOnRefreshListener(new RefreshVideoListener());

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, COLUMNS_ON_PAGE));
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.default_margin)));
        moviesAdapter = new MoviesAdapter(MainActivity.this);
        moviesAdapter.setOnLoadMoreListener(new LoadMoreListener());
        moviesAdapter.setListener(new VideoClickListener());
        recyclerView.setAdapter(moviesAdapter);

        setupActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new OnSearchVideoQuery());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkController.cancelByTag(TAG);
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.app_name);
    }

    private void refreshResults() {
        networkController.clearCache();
        initNewSearch(queryTitle);
    }

    private void sendRequest(String query, int currentPage) {
        SearchRequestListener listener = new SearchRequestListener();
        SearchRequest request = new SearchRequest(query, currentPage, paginationHelper.getItemsOnPage(), listener, listener);
        request.setTag(TAG);
        networkController.addToRequestQueue(request);
    }

    private void showProgress() {
        refreshLayout.setRefreshing(true);
    }

    private void hideProgress() {
        refreshLayout.setRefreshing(false);
    }

    private void initNewSearch(String query) {
        paginationHelper.setCurrentPage(0);
        paginationHelper.setTotalItems(0);

        moviesAdapter.clearItems();

        showProgress();

        sendRequest(query, paginationHelper.getCurrentPage());
    }

    private class VideoClickListener implements MoviesAdapter.VideoClickListener {

        @Override
        public void onClick(Preview preview) {
            if (preview != null) {
                startActivity(VideoPreviewActivity.getIntent(MainActivity.this, preview.getNid()));
            } else {
                AppUtils.showMessage("Video preview id is empty");
            }
        }
    }

    private class LoadMoreListener implements OnLoadMoreListener {
        @Override
        public void onLoadMore() {
            sendRequest(queryTitle, paginationHelper.getNextPage());
        }
    }

    private class OnSearchVideoQuery implements SearchView.OnQueryTextListener {

        @Override
        public boolean onQueryTextSubmit(String query) {
            queryTitle = query;

            getSupportActionBar().setTitle(query);

            initNewSearch(query);

            searchView.clearFocus();
            searchItem.collapseActionView();
            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }

    }

    private class SearchRequestListener implements Response.Listener<SearchResponse>, Response.ErrorListener {

        @Override
        public void onResponse(SearchResponse response) {
            hideProgress();
            moviesAdapter.addItems(response.getTitles());

            paginationHelper.setTotalItems(response.getTotalCount());
            paginationHelper.setCurrentPage(response.getCurrentPage());

            moviesAdapter.setItemsAvailable(paginationHelper.isLoadingAvailable());
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
            AppUtils.showMessage(error.getMessage());
        }
    }

    private class RefreshVideoListener implements SwipeRefreshLayout.OnRefreshListener {
        @Override
        public void onRefresh() {
            if (!TextUtils.isEmpty(queryTitle)) {
                refreshResults();
            } else {
                hideProgress();
            }
        }
    }
}

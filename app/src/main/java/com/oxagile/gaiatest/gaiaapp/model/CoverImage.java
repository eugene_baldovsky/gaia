package com.oxagile.gaiatest.gaiaapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Contains data from <coverart_image>
 */

public class CoverImage {
    @SerializedName("hdtv_190x266")
    private String hdTv190;
    @SerializedName("hdtv_250x350")
    private String hdTv250;
    @SerializedName("hdtv_385x539")
    private String hdTv385;
    @SerializedName("hdtv_500x700")
    private String hdTv500;
    @SerializedName("roku_158x204")
    private String roku158;
    @SerializedName("roku_214x306")
    private String roku214;

    public String getHdTv190() {
        return hdTv190;
    }

    public String getHdTv250() {
        return hdTv250;
    }

    public String getHdTv385() {
        return hdTv385;
    }

    public String getHdTv500() {
        return hdTv500;
    }

    public String getRoku158() {
        return roku158;
    }

    public String getRoku214() {
        return roku214;
    }
}

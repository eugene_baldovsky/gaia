package com.oxagile.gaiatest.gaiaapp.network.error;

import com.android.volley.VolleyError;
import com.oxagile.gaiatest.gaiaapp.R;
import com.oxagile.gaiatest.gaiaapp.app.GaiaApp;

/**
 * No connection error
 */
public class NoInternetConnectionError extends ConnectionError {
    public NoInternetConnectionError() {
        super(GaiaApp.getInstance().getResources().getString(R.string.msg_noconnection));
    }

}

package com.oxagile.gaiatest.gaiaapp.network.reponse;

import com.oxagile.gaiatest.gaiaapp.model.ContentTypeTotals;
import com.oxagile.gaiatest.gaiaapp.model.Title;

import java.util.List;

/**
 * Contains data from <response> tag for response with list of videos
 */
public class SearchResponse extends PagedResponse{
    private List<Title> titles;
    private ContentTypeTotals contentTypeTotals;

    public SearchResponse() {
    }

    public void setTitles(List<Title> titles) {
        this.titles = titles;
    }

    public List<Title> getTitles() {
        return titles;
    }

    public ContentTypeTotals getContentTypeTotals() {
        return contentTypeTotals;
    }
}

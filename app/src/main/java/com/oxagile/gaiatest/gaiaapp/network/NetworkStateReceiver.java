package com.oxagile.gaiatest.gaiaapp.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.oxagile.gaiatest.gaiaapp.app.NetworkController;

/**
 * Receives information abour network state
 */
public class NetworkStateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getExtras() != null) {
            final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

            boolean isConnected = false;
            if (ni != null && ni.isConnectedOrConnecting()) {
                isConnected = true;
            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                isConnected = false;
            }

            NetworkController.getInstance(context).setInternetConnected(isConnected);
        }
    }
}

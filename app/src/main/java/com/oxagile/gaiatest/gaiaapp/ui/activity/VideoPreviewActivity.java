package com.oxagile.gaiatest.gaiaapp.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.oxagile.gaiatest.gaiaapp.R;
import com.oxagile.gaiatest.gaiaapp.app.NetworkController;
import com.oxagile.gaiatest.gaiaapp.network.reponse.MediaResponse;
import com.oxagile.gaiatest.gaiaapp.network.request.MediaRequest;
import com.oxagile.gaiatest.gaiaapp.utils.AppUtils;

public class VideoPreviewActivity extends BaseActivity {
    private static final String TAG = "VideoPreviewActivity";
    private static final String NID_KEY = "NID_KEY";
    public static final String POSITION = "Position";
    public static final int DELAY_BEFORE_ON_ERROR_CLOSING = 500;

    private long nid;
    private MediaController mediaControls;
    private int position = 0;
    private VideoView videoView;
    private NetworkController networkController;
    private boolean isMediaResponseReceived;

    public static Intent getIntent(Context context, long nid) {
        Intent intent = new Intent(context, VideoPreviewActivity.class);
        intent.putExtra(NID_KEY, nid);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_preview);

        networkController = NetworkController.getInstance(this);

        nid = getIntent().getLongExtra(NID_KEY, 0);

        videoView = (VideoView) findViewById(R.id.videoView);
        configVideoView();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //we use onRestoreInstanceState in order to play the video playback from the stored position
        position = savedInstanceState.getInt(POSITION);
        videoView.seekTo(position);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isMediaResponseReceived) {
            sendMediaRequest();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkController.cancelByTag(TAG);
    }

    private void playVideo(String url) {
        try {
            //set the uri of the video to be played
            videoView.setVideoURI(Uri.parse(url));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            showErrorAndClose(getString(R.string.media_play_error));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //we use onSaveInstanceState in order to store the video playback position for orientation change
        savedInstanceState.putInt(POSITION, videoView.getCurrentPosition());
        videoView.pause();
    }

    private void configVideoView() {
        //set the media controller buttons
        if (mediaControls == null) {
            mediaControls = new MediaController(this);
        }
        //set the media controller in the VideoView

        videoView.setMediaController(mediaControls);

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e(TAG, "onError: mediaError what = [" + what + "], extra = [" + extra + "]");
                showErrorAndClose(getString(R.string.media_play_error));
                return false;
            }
        });

        videoView.requestFocus();
        //we also set an setOnPreparedListener in order to know when the video file is ready for playback
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                //if we have a position on savedInstanceState, the video playback should start from here
                videoView.seekTo(0);
                if (position == 0) {
                    videoView.start();
                } else {
                    //if we come from a resumed activity, video playback will be paused
                    videoView.pause();
                }
            }
        });
    }

    private void showErrorAndClose(String message) {
        AppUtils.showMessage(message);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, DELAY_BEFORE_ON_ERROR_CLOSING);
    }

    private void sendMediaRequest() {
        MediaListener listener = new MediaListener();
        MediaRequest request = new MediaRequest(nid, listener, listener);
        networkController.addToRequestQueue(request);
    }

    private class MediaListener implements Response.Listener<MediaResponse>, Response.ErrorListener {

        @Override
        public void onResponse(MediaResponse response) {
            isMediaResponseReceived = true;

            playVideo(response.getMediaUrls().getBcHLS());
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, "onErrorResponse: ", error);
            showErrorAndClose(getString(R.string.media_info_request_error));
        }
    }
}

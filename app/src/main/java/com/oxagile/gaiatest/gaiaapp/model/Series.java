package com.oxagile.gaiatest.gaiaapp.model;

/**
 * Contains data from <series>
 */
public class Series {
    private int nid;
    private String title;
    private String path;

    public int getNid() {
        return nid;
    }

    public String getTitle() {
        return title;
    }

    public String getPath() {
        return path;
    }
}

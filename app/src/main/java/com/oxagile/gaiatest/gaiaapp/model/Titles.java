package com.oxagile.gaiatest.gaiaapp.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Contains data from <titles>
 */
public class Titles {

    private List<Title> titles;

    public Titles() {
        titles = new LinkedList<>();
    }

    public Titles(List<Title> titles) {
        this.titles = titles;
    }

    public List<Title> getTitles(){
        return titles;
    }
}

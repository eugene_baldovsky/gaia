package com.oxagile.gaiatest.gaiaapp.network.error;

import com.android.volley.VolleyError;
import com.oxagile.gaiatest.gaiaapp.R;
import com.oxagile.gaiatest.gaiaapp.app.GaiaApp;

/**
 * Common class for connection errors
 */
public class ConnectionError extends VolleyError {
    private int code = -1;

    public ConnectionError() {
        super();
    }

    public ConnectionError(int errorCode) {
        super();

        code = errorCode;
    }

    public ConnectionError(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return getErrorMessageByCode(code);
    }

    /*
     * return error message
     */
    protected String getErrorMessageByCode(int errorCode){
        switch(errorCode){
            default:
                return "unknown connection error";
        }
    }
}

package com.oxagile.gaiatest.gaiaapp.network.request;

import com.android.volley.Response;
import com.oxagile.gaiatest.gaiaapp.network.UrlConstants;
import com.oxagile.gaiatest.gaiaapp.network.reponse.SearchResponse;

import java.util.Map;

/**
 * Search request
 */
public class SearchRequest extends BaseRequest<SearchResponse> {
    private static final String METHOD_NAME = "search";
    private static final String CATEGORY_NAME = "videos";

    private static final String URI_BASE = UrlConstants.PUBLIC_ENDPOINT + "/" + CATEGORY_NAME + "/"
            + METHOD_NAME + "/%s?p=%d&pp=%d";

    public SearchRequest(String query, int currentPage, int pageSize, Response.Listener<SearchResponse> listener, Response.ErrorListener errorListener) {
        super(Method.GET,
                SearchResponse.class,
                String.format(URI_BASE, query, currentPage, pageSize),
                null,
                listener, errorListener);
    }
}
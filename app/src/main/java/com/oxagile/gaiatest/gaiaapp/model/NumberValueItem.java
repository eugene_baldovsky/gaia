package com.oxagile.gaiatest.gaiaapp.model;

/**
 * Contains data for object with "value" field as a number
 */

public class NumberValueItem {
    private int value;

    public int getValue() {
        return value;
    }
}

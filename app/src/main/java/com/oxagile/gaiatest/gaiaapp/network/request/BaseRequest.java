package com.oxagile.gaiatest.gaiaapp.network.request;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.JsonSyntaxException;
import com.oxagile.gaiatest.gaiaapp.parser.ParseManager;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Base class for request
 */
public abstract class BaseRequest<T> extends JsonRequest<T> {
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String ACCEPT = "Accept";
    private final Class<T> clazz;

    public BaseRequest(int methodType, Class<T> clazz, String url, String requestBody,
                       Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(methodType, url, requestBody, listener, errorListener);
        this.clazz = clazz;
    }

    @Override
    protected Map<String, String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        return params;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put(CONTENT_TYPE, "application/json");
        headers.put(ACCEPT, "application/json");
        return headers;
    }

    @Override
    public String getCacheKey() {
        return super.getCacheKey();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(ParseManager.getInstance().fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}

package com.oxagile.gaiatest.gaiaapp.ui.listener;

/**
 * Created by Eugene on 21-Nov-16.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}

package com.oxagile.gaiatest.gaiaapp.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Contains data from <contentTypeTotals>
 */
public class ContentTypeTotals {
    private int article;
    private int series;
    private int video;

    public ContentTypeTotals(){}

    public int getArticle() {
        return article;
    }

    public int getSeries() {
        return series;
    }

    public int getVideo() {
        return video;
    }
}

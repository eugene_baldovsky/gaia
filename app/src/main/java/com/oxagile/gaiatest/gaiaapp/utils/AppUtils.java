package com.oxagile.gaiatest.gaiaapp.utils;

import android.util.Log;
import android.widget.Toast;

import com.oxagile.gaiatest.gaiaapp.app.GaiaApp;

/**
 * Applciation utils
 */
public class AppUtils {
    public static void Log(String tag, String message){
        Log.d(tag, message);
    }

    public static void showMessage(String message){
        Toast.makeText(GaiaApp.getInstance(), message, Toast.LENGTH_SHORT).show();
    }

    public static void showMessage(int stringRes){
        String message = GaiaApp.getInstance().getResources().getString(stringRes);
        showMessage(message);
    }
}

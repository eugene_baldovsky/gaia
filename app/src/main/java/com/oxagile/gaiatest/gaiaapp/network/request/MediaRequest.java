package com.oxagile.gaiatest.gaiaapp.network.request;

import com.android.volley.Response;
import com.oxagile.gaiatest.gaiaapp.network.UrlConstants;
import com.oxagile.gaiatest.gaiaapp.network.reponse.MediaResponse;

public class MediaRequest extends BaseRequest<MediaResponse> {
    private static final String URI_BASE = UrlConstants.PUBLIC_ENDPOINT + "/media/";

    public MediaRequest(long nid, Response.Listener<MediaResponse> listener, Response.ErrorListener errorListener) {
        super(Method.GET, MediaResponse.class, URI_BASE + nid, null, listener, errorListener);
    }
}

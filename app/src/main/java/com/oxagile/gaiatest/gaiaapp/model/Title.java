package com.oxagile.gaiatest.gaiaapp.model;

import android.support.v4.util.Pair;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Contains data from <title>
 */
public class Title {
    private Fields fields;
    private String title;
    @SerializedName("comment_count")
    private int commentCount;
    private long created;
    private String displayType;
    @SerializedName("is_new")
    private boolean isNew;
    private int nid;
    private String path;
    @SerializedName("product_type")
    private String productType;
    private String promote;
    private String type;
    @SerializedName("total_episodes")
    private int totalEpisodes;
    @SerializedName("total_seasons")
    private int totalSeasons;
    @SerializedName("coverart_image")
    private CoverImage coverImages;
    @SerializedName("fivestar")
    private FiveStar fiveStarInfo;
    private Series series;
    private Preview preview;

    public String getTitle(){
        return title;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public long getCreated() {
        return created;
    }

    public String getDisplayType() {
        return displayType;
    }

    public boolean getIsNew() {
        return isNew;
    }

    public int getNid() {
        return nid;
    }

    public String getPath() {
        return path;
    }

    public String getProductType() {
        return productType;
    }

    public String getPromote() {
        return promote;
    }

    public String getType() {
        return type;
    }

    public int getTotalEpisodes() {
        return totalEpisodes;
    }

    public int getTotalSeasons() {
        return totalSeasons;
    }

    public Fields getFields() {
        return fields;
    }

    public CoverImage getCoverImages() {
        return coverImages;
    }

    public boolean isNew() {
        return isNew;
    }

    public FiveStar getFiveStarInfo() {
        return fiveStarInfo;
    }

    public Series getSeries() {
        return series;
    }

    public Preview getPreview() {
        return preview;
    }
}

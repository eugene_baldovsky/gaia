package com.oxagile.gaiatest.gaiaapp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.oxagile.gaiatest.gaiaapp.R;
import com.oxagile.gaiatest.gaiaapp.model.Preview;
import com.oxagile.gaiatest.gaiaapp.model.FiveStar;
import com.oxagile.gaiatest.gaiaapp.model.Series;
import com.oxagile.gaiatest.gaiaapp.model.StringValueItem;
import com.oxagile.gaiatest.gaiaapp.model.Title;
import com.oxagile.gaiatest.gaiaapp.ui.listener.OnLoadMoreListener;

import java.util.ArrayList;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private List<Title> items = new ArrayList<>();
    private LayoutInflater inflater;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isItemsAvailable;
    private Context context;
    private VideoClickListener listener;

    public interface VideoClickListener {
        void onClick(Preview preview);
    }

    public MoviesAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setItemsAvailable(boolean itemsAvailable) {
        isItemsAvailable = itemsAvailable;
    }

    public void setListener(VideoClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return position < items.size() ? VIEW_TYPE_ITEM : VIEW_TYPE_LOADING;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = inflater.inflate(R.layout.video_item, parent, false);
            return new VideoViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = inflater.inflate(R.layout.loading_item, parent, false);
            return new LoadingViewHolder(view);
        } else {
            throw new IllegalArgumentException("Passed view type is not supported: viewType = " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                Title title = items.get(position);
                ((VideoViewHolder) holder).setData(title);
                break;
            case VIEW_TYPE_LOADING:
                ((LoadingViewHolder) holder).setData();
                break;
        }
    }

    @Override
    public int getItemCount() {
        return items.size() + (isItemsAvailable ? 1 : 0);
    }

    public void addItems(List<Title> titles) {
        items.addAll(titles);

        notifyDataSetChanged();
    }

    public void clearItems() {
        isItemsAvailable = false;
        items = new ArrayList<>();

        notifyDataSetChanged();
    }

    class VideoViewHolder extends RecyclerView.ViewHolder {
        private final TitleClickListener clickListener = new TitleClickListener();
        private final TextView tvTitle;
        private final TextView tvSeries;
        private final TextView tvPositive;
        private final TextView tvNegative;
        private final ImageView imgCover;
        private final TextView tvEpisode;
        private final TextView tvDuration;

        public VideoViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvSeries = (TextView) itemView.findViewById(R.id.tvSeries);
            tvPositive = (TextView) itemView.findViewById(R.id.tvPositive);
            tvNegative = (TextView) itemView.findViewById(R.id.tvNegative);
            tvEpisode = (TextView) itemView.findViewById(R.id.tvEpisode);
            tvDuration = (TextView) itemView.findViewById(R.id.tvDuration);
            imgCover = (ImageView) itemView.findViewById(R.id.imgCover);
        }

        public void setData(Title title) {
            tvTitle.setText(title.getTitle());

            //set episode
            List<StringValueItem> episode = title.getFields().getEpisode();
            if(episode != null && episode.size() > 0){

                String episodeNumber = episode.get(0).getValue();
                if(episodeNumber != null) {
                    String episodeInfo = String.format(context.getString(R.string.episode_template), episode.get(0).getValue());
                    tvEpisode.setText(episodeInfo);
                    tvEpisode.setVisibility(View.VISIBLE);
                } else {
                    tvEpisode.setVisibility(View.GONE);
                }
            }

            //set series
            Series series = title.getSeries();
            if(series != null) {
                tvSeries.setText(series.getTitle());
            } else {
                tvSeries.setVisibility(View.GONE);
            }

            //set likes
            FiveStar socialInfo = title.getFiveStarInfo();
            if(socialInfo != null) {
                int dislikeCount = socialInfo.getDownCount().getValue();
                int likeCount = socialInfo.getUpCount().getValue();
                int commonAmount = dislikeCount + likeCount;
                tvPositive.setText(Math.round(((double) likeCount/commonAmount) * 100) + "%");
                tvNegative.setText(Math.round(((double) dislikeCount/commonAmount) * 100) + "%");
            }

            //set image
            Glide.with(context).load(title.getCoverImages().getHdTv250())
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgCover);

            clickListener.setPreview(title.getPreview());

            imgCover.setOnClickListener(clickListener);
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            progressBar.setIndeterminate(true);
        }

        public void setData() {
            if (onLoadMoreListener != null) {
                onLoadMoreListener.onLoadMore();
            }
        }
    }

    private class TitleClickListener implements View.OnClickListener {
        private Preview preview;

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onClick(preview);
            }
        }

        public void setPreview(Preview preview) {
            this.preview = preview;
        }
    }
}

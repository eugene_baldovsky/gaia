package com.oxagile.gaiatest.gaiaapp.parser;

import com.google.gson.Gson;

/**
 * Parse Manager
 */
public class ParseManager {

    private Gson gson = new Gson();

    private static ParseManager instance;

    public static ParseManager getInstance() {
        if (instance == null) {
            synchronized (ParseManager.class) {
                if (instance == null) {
                    instance = new ParseManager();
                }
            }
        }
        return instance;
    }

    private ParseManager() {
    }

    public <T> T fromJson(final String json, final Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }
}
